
const form = document.querySelector('.password-form');

function clickHandler(icon, input){
    if(input.getAttribute('type') == 'password') {
        input.removeAttribute('type');
        input.setAttribute('type', 'text');
        icon.className = 'far fa-eye-slash icon-password';
       }
       else {
        input.removeAttribute('type');
        input.setAttribute('type', 'password');
        icon.className = 'far fa-eye icon-password';
       }
}

function validPassword(){
    let passFirst = document.getElementById('password-1');
    let passSecond = document.getElementById('password-2');
    let warning = document.querySelector('.text');
    
    if (passFirst.value === passSecond.value){
        warning.classList.remove('active');
        alert('You are welcome');
    } 
    else {
        warning.classList.add('active');
    }
}

form.addEventListener('click', function(event){
    if (event.target.classList.contains('icon-password')){
        let input = event.target.previousElementSibling;
        clickHandler(event.target, input); 
    }
    else if (event.target.classList.contains('btn')){ 
        validPassword();
    }
})
